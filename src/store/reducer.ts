import { createReducer } from '@reduxjs/toolkit';
import { addCharacter, evaluate, evaluateFailed, evaluateSuccess, removePreviousCharacter, reset } from './actions';
import { initialState } from './state';

export const appReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(removePreviousCharacter, (state) => ({
      ...state,
      error: null,
      currentFormula: state.currentFormula.slice(0, -1),
    }))
    .addCase(reset, () => ({ ...initialState }))
    .addCase(addCharacter, (state, { payload }) => ({
      ...state,
      error: null,
      currentFormula: state.currentFormula.concat(payload),
    }))
    .addCase(evaluate, (state) => ({ ...state, isLoading: true }))
    .addCase(evaluateSuccess, (state, { payload }) => ({
      ...state,
      error: null,
      isLoading: false,
      currentFormula: payload,
    }))
    .addCase(evaluateFailed, (state, { payload }) => ({
      ...state,
      isLoading: false,
      error: payload,
    })),
);
