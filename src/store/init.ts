import { Epic, createEpicMiddleware } from 'redux-observable';
import { AnyAction, Reducer, Store, createStore, applyMiddleware } from 'redux';
import { AppState } from './state';

export const initAndGetStore = ({
  reducer,
  rootEpic,
}: {
  reducer: Reducer<AppState, AnyAction>;
  rootEpic: Epic;
}): Store<AppState> => {
  const epicMiddleware = createEpicMiddleware();

  const store = createStore(reducer, applyMiddleware(epicMiddleware));
  epicMiddleware.run(rootEpic);
  return store;
};
