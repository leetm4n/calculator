import { ErrorCode } from '../backend/framework/errors/error-code';

export type AppState = {
  currentFormula: string;
  isLoading: boolean;
  error: ErrorCode | null;
};

export const initialState: AppState = {
  currentFormula: '',
  isLoading: false,
  error: null,
};
