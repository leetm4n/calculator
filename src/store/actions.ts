import { createAction } from '@reduxjs/toolkit';
import { ErrorCode } from '../backend/framework/errors/error-code';

export const removePreviousCharacter = createAction('[Calculator] Remove Previous Character');
export const addCharacter = createAction<string>('[Calculator] Add character');
export const reset = createAction('[Calculator] Reset');
export const evaluate = createAction('[Calculator] Evaluate');
export const evaluateSuccess = createAction<string>('[Calculator] Evaluate Success');
export const evaluateFailed = createAction<ErrorCode>('[Calculator] Evaluate Failed');
