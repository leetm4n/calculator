import { ErrorCode } from '../backend/framework/errors/error-code';
import { AppState } from './state';

export const selectCurrentFormula = (state: AppState): string => state.currentFormula;
export const selectError = (state: AppState): ErrorCode | null => state.error;
export const selectIsLoading = (state: AppState): boolean => state.isLoading;
