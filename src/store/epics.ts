import { ActionsObservable, StateObservable, Epic } from 'redux-observable';
import { Action } from 'redux';
import { ajax } from 'rxjs/ajax';
import { of, fromEvent, Observable } from 'rxjs';
import { filter, withLatestFrom, mergeMap, map, catchError } from 'rxjs/operators';
import { addCharacter, evaluate, evaluateFailed, evaluateSuccess, removePreviousCharacter } from './actions';
import { AppState } from './state';
import { EvaluateFormulaRequestBody, EvaluateFormulaResponse } from '../backend/endpoints/evaluate/evaluate-endpoint';
import { ErrorCode } from '../backend/framework/errors/error-code';

const headers = { 'content-type': 'application/json' };

const post = <TResponseBody, TRequestBody>({
  url,
  body,
}: {
  url: string;
  body: TRequestBody;
}): Observable<{ response: TResponseBody }> =>
  ajax
    .post(url, JSON.stringify(body), headers)
    .pipe(map((ajaxResponse) => ({ response: ajaxResponse.response as TResponseBody })));
type ErrorResponse = { response: { errorCode: ErrorCode } };

export const evaluateFormula$: Epic = (actions$: ActionsObservable<Action>, state$: StateObservable<AppState>) =>
  actions$.pipe(
    filter(evaluate.match),
    withLatestFrom(state$),
    mergeMap(([_, state]) =>
      post<EvaluateFormulaResponse, EvaluateFormulaRequestBody>({
        url: '/api/evaluate',
        body: { formula: state.currentFormula },
      }).pipe(
        map(({ response }) => evaluateSuccess(response.result)),
        catchError((error: ErrorResponse) => {
          return of(evaluateFailed(error.response.errorCode));
        }),
      ),
    ),
  );

type InputKeyActionMap = Record<string, () => Action>;

const validInputs: InputKeyActionMap = {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Backspace: () => removePreviousCharacter(),
  '(': () => addCharacter('('),
  ')': () => addCharacter(')'),
  '0': () => addCharacter('0'),
  '1': () => addCharacter('1'),
  '2': () => addCharacter('2'),
  '3': () => addCharacter('3'),
  '4': () => addCharacter('4'),
  '5': () => addCharacter('5'),
  '6': () => addCharacter('6'),
  '7': () => addCharacter('7'),
  '8': () => addCharacter('8'),
  '9': () => addCharacter('9'),
  '+': () => addCharacter('+'),
  '/': () => addCharacter('/'),
  '*': () => addCharacter('*'),
  '-': () => addCharacter('-'),
  '.': () => addCharacter('.'),
  ',': () => addCharacter('.'),
  '=': () => evaluate(),
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Enter: () => evaluate(),
};

export const keyPressDown$: Epic = () =>
  fromEvent<KeyboardEvent>(window, 'keydown').pipe(
    filter((e) => !!validInputs[e.key]),
    map((e) => validInputs[e.key]()),
  );
