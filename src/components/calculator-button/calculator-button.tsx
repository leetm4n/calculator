import React, { FC } from 'react';
import { Button } from '@material-ui/core';
import { CalculatorButtonProps } from './calculator-button.props';
import { calculatorStyle } from './calculator-button.style';

export const CalculatorButton: FC<CalculatorButtonProps> = ({ displayedValue, onPress, isDisabled }) => (
  <Button style={calculatorStyle} disabled={isDisabled} variant="contained" onClick={onPress}>
    {displayedValue}
  </Button>
);
