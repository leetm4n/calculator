export type CalculatorButtonProps = {
  displayedValue: string;
  onPress: () => void;
  isDisabled: boolean;
};
