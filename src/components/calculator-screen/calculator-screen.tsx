import React, { FC } from 'react';
import { TextField } from '@material-ui/core';
import { CalculatorScreenProps } from './calculator-screen.props';

export const CalculatorScreen: FC<CalculatorScreenProps> = ({ value, errorMessage }) => (
  <TextField variant="outlined" value={value} error={!!errorMessage} helperText={errorMessage}></TextField>
);
