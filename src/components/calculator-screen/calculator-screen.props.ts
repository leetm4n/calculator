export type CalculatorScreenProps = {
  value: string;
  errorMessage: string | null;
};
