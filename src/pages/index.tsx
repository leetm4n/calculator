import Head from 'next/head';
import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from '@material-ui/core';
import { CalculatorButtonProps } from '../components/calculator-button/calculator-button.props';
import { addCharacter, evaluate, removePreviousCharacter, reset } from '../store/actions';
import { CalculatorButton } from '../components/calculator-button/calculator-button';
import { selectCurrentFormula, selectError, selectIsLoading } from '../store/selectors';
import { CalculatorScreen } from '../components/calculator-screen/calculator-screen';

export default function Home(): JSX.Element {
  const dispatch = useDispatch();
  const currentFormula = useSelector(selectCurrentFormula);
  const error = useSelector(selectError);
  const isLoading = useSelector(selectIsLoading);

  const buttonRows: CalculatorButtonProps[][] = useMemo(
    () => [
      [
        {
          displayedValue: '(',
          onPress: () => {
            dispatch(addCharacter('('));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: ')',
          onPress: () => {
            dispatch(addCharacter(')'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: 'CE',
          onPress: () => {
            dispatch(removePreviousCharacter());
          },
          isDisabled: isLoading || !currentFormula.length,
        },
        {
          displayedValue: 'AC',
          onPress: () => {
            dispatch(reset());
          },
          isDisabled: isLoading || !currentFormula.length,
        },
      ],
      [
        {
          displayedValue: '7',
          onPress: () => {
            dispatch(addCharacter('7'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '8',
          onPress: () => {
            dispatch(addCharacter('8'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '9',
          onPress: () => {
            dispatch(addCharacter('9'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '/',
          onPress: () => {
            dispatch(addCharacter('/'));
          },
          isDisabled: isLoading,
        },
      ],
      [
        {
          displayedValue: '4',
          onPress: () => {
            dispatch(addCharacter('4'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '5',
          onPress: () => {
            dispatch(addCharacter('5'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '6',
          onPress: () => {
            dispatch(addCharacter('6'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '*',
          onPress: () => {
            dispatch(addCharacter('*'));
          },
          isDisabled: isLoading,
        },
      ],
      [
        {
          displayedValue: '1',
          onPress: () => {
            dispatch(addCharacter('1'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '2',
          onPress: () => {
            dispatch(addCharacter('2'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '3',
          onPress: () => {
            dispatch(addCharacter('3'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '-',
          onPress: () => {
            dispatch(addCharacter('-'));
          },
          isDisabled: isLoading,
        },
      ],
      [
        {
          displayedValue: '0',
          onPress: () => {
            dispatch(addCharacter('0'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '.',
          onPress: () => {
            dispatch(addCharacter('.'));
          },
          isDisabled: isLoading,
        },
        {
          displayedValue: '=',
          onPress: () => {
            dispatch(evaluate());
          },
          isDisabled: isLoading || !currentFormula.length,
        },
        {
          displayedValue: '+',
          onPress: () => {
            dispatch(addCharacter('+'));
          },
          isDisabled: isLoading,
        },
      ],
    ],
    [dispatch, isLoading, currentFormula, error],
  );

  const errorMessage = error?.toLowerCase().split('_').join(' ') ?? null;

  return (
    <>
      <div className="container">
        <Head>
          <title>Calculator</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Grid container direction="column" style={{ maxWidth: '320px', padding: '20px' }}>
          <CalculatorScreen value={currentFormula} errorMessage={errorMessage}></CalculatorScreen>

          {buttonRows.map((buttonRow, index) => (
            <Grid container direction="row" key={index}>
              {buttonRow.map(({ displayedValue, onPress, isDisabled }) => (
                <CalculatorButton
                  key={displayedValue}
                  displayedValue={displayedValue}
                  onPress={onPress}
                  isDisabled={isDisabled}
                ></CalculatorButton>
              ))}
            </Grid>
          ))}
        </Grid>
      </div>
    </>
  );
}
