import React from 'react';
import { Provider } from 'react-redux';
import { combineEpics } from 'redux-observable';
import type { AppProps } from 'next/app';
import { evaluateFormula$, keyPressDown$ } from '../store/epics';
import { appReducer } from '../store/reducer';
import { initAndGetStore } from '../store/init';

const store = initAndGetStore({
  reducer: appReducer,
  rootEpic: process.browser ? combineEpics(evaluateFormula$, keyPressDown$) : evaluateFormula$,
});

function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default App;
