import contentType from 'content-type';
import { FastifyInstance, HTTPMethods } from 'fastify';
import { NextApiRequest, NextApiResponse } from 'next';
import getRawBody from 'raw-body';
import { config as appConfig } from '../../config';
import { calculatorExact } from '../../backend/domain/calculator/calculator-exact';
import { evaluateEndpointFactory } from '../../backend/endpoints/evaluate/evaluate-endpoint';
import { statusEndpointFactory } from '../../backend/endpoints/status/status-endpoint';
import { Endpoint } from '../../backend/framework/endpoint/endpoint';
import { errorMapper } from '../../backend/framework/errors/error-map';
import { serverFastifyFactory } from '../../backend/framework/fastify/fastify-server';
import { loggerPinoFactory } from '../../backend/framework/logger/logger-pino';
import { evaluateFormulaUseCaseFactory } from '../../backend/use-cases/evalulate-formula-use-case';

export const config = {
  api: {
    bodyParser: false,
  },
};

let fastifyInstance: FastifyInstance | null = null;
export const setupFastifyInstance = async (): Promise<FastifyInstance> => {
  const logger = loggerPinoFactory({
    name: 'calculator-api',
    version: '0.0.1',
    level: 'info',
  });

  const endpoints = [
    statusEndpointFactory(),
    evaluateEndpointFactory({ evaluateFormulaUseCase: evaluateFormulaUseCaseFactory({ calculator: calculatorExact }) }),
  ];
  const app = await serverFastifyFactory({
    host: appConfig.host,
    prefix: '/api',
    logger,
    endpoints: endpoints as Endpoint[],
    errorHandler: errorMapper,
  });
  await app.oas();
  await app.ready();

  return app;
};

export default async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
  // lazy load on first startup
  if (!fastifyInstance) {
    fastifyInstance = await setupFastifyInstance();
  }

  // transform next.js to fastify and back
  const response = await fastifyInstance.inject({
    method: req.method as HTTPMethods,
    url: req.url,
    payload: await getRawBody(req, {
      length: req.headers['content-length'] || 0,
      limit: '1mb',
      encoding: req.headers['content-type'] ? contentType.parse(req).parameters.charset : undefined,
    }),
    headers: req.headers,
  });

  Object.entries(response.headers).forEach(([key, value]) => res.setHeader(key, getHeaderValue(value)));

  res.status(response.statusCode).send(response.rawPayload);
};

const getHeaderValue = (val: string | string[] | undefined | number): string =>
  Array.isArray(val) ? val.join(',') : val ? `${val}` : '';
