declare module 'exact-math' {
  type FormulaConfig = {
    returnString?: boolean;
    decimalChar?: string | string[];
    divChar?: string | string[];
    mulChar?: string | string[];
    eMinus?: number;
    ePlus?: number;
    maxDecimal?: number;
    divideByZeroError?: boolean | Error;
    invalidError?: boolean | Error;
  };
  export function formula(expression: string, config?: FormulaConfig): string;
}
