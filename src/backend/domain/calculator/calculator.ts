export interface Calculator {
  evaluate(expression: string): string;
}
