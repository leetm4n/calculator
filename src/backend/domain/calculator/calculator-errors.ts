import { ErrorCode } from '../../framework/errors/error-code';

export class DivisionByZeroError extends Error {
  constructor() {
    super(ErrorCode.DIVISION_BY_ZERO);
  }
}

export class InvalidFormulaError extends Error {
  constructor() {
    super(ErrorCode.INVALID_FORMULA);
  }
}
