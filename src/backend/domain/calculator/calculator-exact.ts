import exactMath from 'exact-math';
import { Calculator } from './calculator';
import { DivisionByZeroError, InvalidFormulaError } from './calculator-errors';

export const calculatorExact: Calculator = {
  evaluate: (expression) =>
    exactMath.formula(expression, {
      eMinus: Infinity,
      ePlus: Infinity,
      invalidError: new InvalidFormulaError(),
      divideByZeroError: new DivisionByZeroError(),
    }),
};
