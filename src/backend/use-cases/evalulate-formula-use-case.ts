import { Calculator } from '../domain/calculator/calculator';
import { AsyncUseCase } from '../framework/use-case/async.use-case';

export type EvaluateFormulaUseCase = AsyncUseCase<string, string>;

export const evaluateFormulaUseCaseFactory = ({
  calculator,
}: {
  calculator: Calculator;
}): EvaluateFormulaUseCase => async (formula) => calculator.evaluate(formula);
