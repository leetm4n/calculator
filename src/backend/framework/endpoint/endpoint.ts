/* eslint-disable @typescript-eslint/naming-convention */
import { OpenAPIV3 } from 'openapi-types';

export enum EndpointMethod {
  OPTIONS = 'OPTIONS',
  HEAD = 'HEAD',
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE',
}
export type EndpointRoute = string;
export type EndpointTag = { name: string; tag: string };
export type EndpointHandlerInput<
  THeaders extends object = {},
  TQuery extends object = {},
  TParams extends object = {},
  TBody extends object = {}
> = {
  headers: THeaders;
  query: TQuery;
  params: TParams;
  body: TBody;
};
export type EndpointSchema = {
  summary?: string;
  description?: string;
  tags?: string[];
  headers?: OpenAPIV3.NonArraySchemaObject;
  query?: OpenAPIV3.NonArraySchemaObject;
  params?: OpenAPIV3.NonArraySchemaObject;
  body?: OpenAPIV3.NonArraySchemaObject;
  response?: { [key: string]: OpenAPIV3.NonArraySchemaObject | OpenAPIV3.ArraySchemaObject };
};
export type EndpointHandlerResponse<TResponse> = {
  status: number;
  response: TResponse;
  headers?: { [key: string]: string };
};
export type Endpoint<
  THeaders extends object = {},
  TQuery extends object = {},
  TParams extends object = {},
  TBody extends object = {},
  TResponse extends object = {}
> = {
  method: EndpointMethod;
  route: EndpointRoute;
  schema: EndpointSchema;
  handler: (
    input: EndpointHandlerInput<THeaders, TQuery, TParams, TBody>,
  ) => Promise<EndpointHandlerResponse<TResponse>>;
};
