import { DetailedError } from './detailed-error';
import { ErrorCode } from './error-code';

export class NotFoundError extends DetailedError {
  constructor(private entityName: string) {
    super(ErrorCode.NOT_FOUND);
  }

  getDetails(): object {
    return {
      entityName: this.entityName,
    };
  }
}

export class ValidationError extends DetailedError {
  constructor(private errors: object) {
    super(ErrorCode.VALIDATION_ERROR);
  }

  getDetails(): object {
    return { errors: this.errors };
  }
}
