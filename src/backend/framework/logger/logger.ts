type LoggerFn = {
  (msg: string, ...args: unknown[]): void;
  (obj: object, msg?: string, ...args: unknown[]): void;
};

export type Logger = {
  info: LoggerFn;
  debug: LoggerFn;
  error: LoggerFn;
  warn: LoggerFn;
};
