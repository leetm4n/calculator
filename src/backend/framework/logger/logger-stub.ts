import { Logger } from './logger';

export const loggerStub: Logger = {
  info: () => {
    return;
  },
  debug: () => {
    return;
  },
  warn: () => {
    return;
  },
  error: () => {
    return;
  },
};
