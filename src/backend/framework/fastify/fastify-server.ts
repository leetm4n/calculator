import fastify, { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from 'fastify';
import oas from 'fastify-oas';
import { Endpoint } from '../endpoint/endpoint';
import { ErrorCode } from '../errors/error-code';
import { ErrorHandler } from '../errors/error-handler';
import { Logger } from '../logger/logger';
import { fastifyErrorHandlingMiddlewareFactory } from './fastify-error-mw';
import { fastifySwaggerFactory } from './fastify-swagger';

export const serverFastifyFactory = async ({
  errorHandler,
  endpoints,
  host,
  prefix,
  logger,
}: {
  errorHandler: ErrorHandler;
  endpoints: Endpoint[];
  host: string;
  prefix: string;
  logger: Logger;
}): Promise<FastifyInstance> => {
  const app = fastify({
    logger: false,
  });

  await app.register(oas, fastifySwaggerFactory({ host, prefix }));
  app.setNotFoundHandler((_, reply) => {
    void reply.status(404).send({ errorCode: ErrorCode.NOT_FOUND });
  });
  app.setErrorHandler(
    fastifyErrorHandlingMiddlewareFactory({
      errorHandler,
    }),
  );

  // Register injected endpoints
  endpoints.forEach((endpoint) => {
    const fastifyRouteOptions: RouteOptions = {
      method: endpoint.method,
      url: `${prefix ? prefix : ''}${endpoint.route}`,
      schema: endpoint.schema,
      handler: wrappedFastifyHandler(endpoint.handler, logger),
    };
    app.route(fastifyRouteOptions);
  });

  return app;
};

const wrappedFastifyHandler = (endpointHandler: Endpoint['handler'], logger: Logger) => async (
  request: FastifyRequest,
  reply: FastifyReply,
) => {
  const endpointArgs = {
    headers: request.headers,
    query: request.query as object,
    params: request.params as object,
    body: request.body as object,
  };
  const endpointResult = await endpointHandler(endpointArgs);

  logger.debug({ request: request.body, response: endpointResult });

  return reply
    .status(endpointResult.status)
    .send(endpointResult.response)
    .headers(endpointResult.headers || {});
};
