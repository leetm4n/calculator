import { FastifyError, FastifyReply, FastifyRequest } from 'fastify';
import { ErrorCode } from '../errors/error-code';
import { ErrorHandler } from '../errors/error-handler';
import { ValidationError } from '../errors/errors';

export const fastifyErrorHandlingMiddlewareFactory = ({
  errorHandler,
}: {
  errorHandler: ErrorHandler;
}): ((error: FastifyError, request: FastifyRequest, reply: FastifyReply) => void) => (err, _, res) => {
  const errorParameters = errorHandler(err.validation ? new ValidationError(err.validation) : err);
  if (errorParameters) {
    return res.status(errorParameters.status).send({
      errorCode: errorParameters.errorCode,
      ...(errorParameters.details ? { details: errorParameters.details } : {}),
    });
  }

  return res.status(500).send({
    errorCode: ErrorCode.INTERNAL_SERVER_ERROR,
  });
};
