import { FastifyOASOptions } from 'fastify-oas';

type FastifySwaggerFactoryArgs = {
  host: string;
  prefix?: string;
};
export const fastifySwaggerFactory = ({ host, prefix }: FastifySwaggerFactoryArgs): FastifyOASOptions => ({
  routePrefix: `${prefix ? prefix : ''}/documentation`,
  exposeRoute: true,
  swagger: {
    info: {
      title: 'API',
      description: 'Simple calculator',
      version: '0.0.0',
    },
    host: host,
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
  },
});
