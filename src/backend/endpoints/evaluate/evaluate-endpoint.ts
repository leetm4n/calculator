import { Endpoint, EndpointMethod } from '../../framework/endpoint/endpoint';
import { EvaluateFormulaUseCase } from '../../use-cases/evalulate-formula-use-case';
import { baseOAErrorResponses } from '../base-response-oapi';

export type EvaluateFormulaResponse = {
  result: string;
};
export type EvaluateFormulaRequestBody = {
  formula: string;
};
export const evaluateEndpointFactory = ({
  evaluateFormulaUseCase,
}: {
  evaluateFormulaUseCase: EvaluateFormulaUseCase;
}): Endpoint<{}, {}, {}, EvaluateFormulaRequestBody, EvaluateFormulaResponse> => ({
  method: EndpointMethod.POST,
  route: '/evaluate',
  schema: {
    summary: 'This endpoint evaluates the given formula',
    description:
      'If formula is correct it gets calculated, otherwise it throws invalid formula or divison by zero error',
    body: {
      type: 'object',
      required: ['formula'],
      additionalProperties: false,
      properties: {
        formula: { type: 'string' },
      },
    },
    response: {
      200: {
        type: 'object',
        description: 'The result of the formula given',
        required: ['result'],
        additionalProperties: false,
        properties: {
          result: { type: 'string' },
        },
      },
      400: baseOAErrorResponses[400],
      500: baseOAErrorResponses[500],
    },
  },
  handler: async ({ body }) => ({
    status: 200,
    response: {
      result: await evaluateFormulaUseCase(body.formula),
    },
  }),
});
