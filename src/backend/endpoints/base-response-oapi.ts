import { OpenAPIV3 } from 'openapi-types';

export const detailedOpenAPIErrorResponse: OpenAPIV3.NonArraySchemaObject = {
  type: 'object',
  description: 'handled error happened during the request',
  required: ['errorCode'],
  properties: {
    errorCode: { type: 'string' },
    details: {
      type: 'object',
      properties: {
        errors: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              keyword: { type: 'string' },
              dataPath: { type: 'string' },
              schemaPath: { type: 'string' },
              message: { type: 'string' },
            },
          },
        },
      },
    },
  },
};

export const baseOAErrorResponses: {
  [key: number]: OpenAPIV3.NonArraySchemaObject;
} = {
  404: {
    ...detailedOpenAPIErrorResponse,
    description: 'resource not found',
  },
  400: {
    ...detailedOpenAPIErrorResponse,
    description: 'user error happened',
  },
  500: {
    ...detailedOpenAPIErrorResponse,
    description: 'internal server error',
  },
};
