import { Endpoint, EndpointMethod } from '../../framework/endpoint/endpoint';
import { baseOAErrorResponses } from '../base-response-oapi';

type Response = {
  status: string;
};

export const statusEndpointFactory = (): Endpoint<{}, {}, {}, {}, Response> => ({
  method: EndpointMethod.GET,
  route: '/status',
  schema: {
    summary: 'This endpoint is responsible for returning the status of the API',
    description: 'This endpoint can only return status: ok or internal server error',
    response: {
      200: {
        type: 'object',
        description: 'API status is ok!',
        required: ['status'],
        additionalProperties: false,
        properties: {
          status: { type: 'string' },
        },
      },
      500: baseOAErrorResponses[500],
    },
  },
  handler: async () => ({
    status: 200,
    response: {
      status: 'ok',
    },
  }),
});
