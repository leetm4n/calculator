import { FastifyInstance } from 'fastify';
import { calculatorExact } from '../domain/calculator/calculator-exact';
import { evaluateEndpointFactory } from '../endpoints/evaluate/evaluate-endpoint';
import { statusEndpointFactory } from '../endpoints/status/status-endpoint';
import { Endpoint, EndpointMethod } from '../framework/endpoint/endpoint';
import { ErrorCode } from '../framework/errors/error-code';
import { errorMapper } from '../framework/errors/error-map';
import { serverFastifyFactory } from '../framework/fastify/fastify-server';
import { loggerStub } from '../framework/logger/logger-stub';
import { evaluateFormulaUseCaseFactory } from '../use-cases/evalulate-formula-use-case';

describe('api tests', () => {
  type TestItem = {
    name: string;
    request: {
      body: object | null;
      url: string;
      method: EndpointMethod;
    };
    expected: {
      body: object;
      statusCode: number;
    };
  };

  const testItems: TestItem[] = [
    {
      name: 'status endpoint',
      request: {
        body: null,
        url: '/status',
        method: EndpointMethod.GET,
      },
      expected: {
        body: {
          status: 'ok',
        },
        statusCode: 200,
      },
    },
    {
      name: 'non existing endpoint',
      request: {
        body: null,
        url: '/nonexisting',
        method: EndpointMethod.GET,
      },
      expected: {
        body: {
          errorCode: ErrorCode.NOT_FOUND,
        },
        statusCode: 404,
      },
    },
    {
      name: 'evaluate endpoint success',
      request: {
        body: { formula: '3*3' },
        url: '/evaluate',
        method: EndpointMethod.POST,
      },
      expected: {
        body: {
          result: '9',
        },
        statusCode: 200,
      },
    },
    {
      name: 'evaluate endpoint invalid formula',
      request: {
        body: { formula: '3*3/' },
        url: '/evaluate',
        method: EndpointMethod.POST,
      },
      expected: {
        body: {
          errorCode: ErrorCode.INVALID_FORMULA,
        },
        statusCode: 400,
      },
    },
    {
      name: 'evaluate endpoint division by zero',
      request: {
        body: { formula: '3/0' },
        url: '/evaluate',
        method: EndpointMethod.POST,
      },
      expected: {
        body: {
          errorCode: ErrorCode.DIVISION_BY_ZERO,
        },
        statusCode: 400,
      },
    },
    {
      name: 'evaluate endpoint validation error',
      request: {
        body: { formula2: '3/3' },
        url: '/evaluate',
        method: EndpointMethod.POST,
      },
      expected: {
        body: {
          errorCode: ErrorCode.VALIDATION_ERROR,
          details: {
            errors: [
              {
                dataPath: '',
                keyword: 'required',
                message: "should have required property 'formula'",
                schemaPath: '#/required',
              },
            ],
          },
        },
        statusCode: 400,
      },
    },
  ];

  let app: FastifyInstance;
  beforeAll(async () => {
    const endpoints = [
      statusEndpointFactory(),
      evaluateEndpointFactory({
        evaluateFormulaUseCase: evaluateFormulaUseCaseFactory({ calculator: calculatorExact }),
      }),
    ];
    app = await serverFastifyFactory({
      host: 'host',
      prefix: '',
      logger: loggerStub,
      endpoints: endpoints as Endpoint[],
      errorHandler: errorMapper,
    });
    await app.oas();
    await app.ready();
  });

  testItems.forEach(({ name, request, expected }) =>
    it(name, async () => {
      const response = await app.inject({
        method: request.method,
        url: request.url,
        ...(request.body ? { payload: JSON.stringify(request.body) } : {}),
        headers: {
          'content-type': 'application/json',
        },
      });

      expect(response.statusCode).toBe(expected.statusCode);
      expect(JSON.parse(response.body)).toEqual(expected.body);
    }),
  );
});
