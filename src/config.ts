import convict from 'convict';

const convictConfig = convict({
  host: {
    doc: 'The hostname',
    format: String,
    default: 'http://localhost:3000',
    env: 'HOST',
  },
});

convictConfig.validate({ allowed: 'strict' });
export const config = convictConfig.getProperties();
export type ConfigObject = typeof config;
