# Simple Calculator app using Node.js and React

## Try it out

The solution is hosted at [Vercel](https://calculator-steel.vercel.app/).
API docs can be found at [API Docs](https://calculator-steel.vercel.app/api/documentation/index.html)

## Running locally

```bash
yarn install
yarn dev
```

## Running API tests locally

```bash
yarn test
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
